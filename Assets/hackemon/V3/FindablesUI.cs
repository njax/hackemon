﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FindablesUI : MonoBehaviour {
	
	public static FindablesUI Instance;
	
	public Text text;
	public string textString;
	
	public void AddText(string message)
	{
		textString += ("\n" + message);
		text.text = textString;
	}
	
	public GameObject dialogGO;
	public Text dialogText;
	public Text dialogTitleText;
	public Button dialogDismissButton;
	
	public GameObject choiceMenuGO; // the parent of choice buttons with a VerticalLayoutGroup
	public GameObject buttonPrefabGO; // the disabled "template" button
	public Text promptText;
	
	public void ShowChoices(string choicePrompt, string[] choices, GameObject messageTargetGO, string messageName)
	{
		HideDialog();
		choiceMenuGO.SetActive(true);
		promptText.text = choicePrompt;
			
		Transform[] children = choiceMenuGO.GetComponentsInChildren<Transform>();
		foreach (Transform childToBeEaten in children) 
		{
			if (childToBeEaten != choiceMenuGO.transform && childToBeEaten != promptText.transform) {
				GameObject.Destroy(childToBeEaten.gameObject);
			}
		}
		foreach (string choiceName in choices)
		{
			GameObject newButtonGO = GameObject.Instantiate(
				buttonPrefabGO,
				buttonPrefabGO.transform.position,
				buttonPrefabGO.transform.rotation,
				buttonPrefabGO.transform.parent
			) as GameObject;
			newButtonGO.SetActive(true);
			newButtonGO.GetComponentInChildren<Text>().text = choiceName;
			string choiceString = choiceName;
			newButtonGO.GetComponentInChildren<Button>().onClick.AddListener(
				() => ClickChoiceButton(choiceString, messageTargetGO, messageName)
			);
		}
	}
	
	public void HideChoices()
	{
		choiceMenuGO.SetActive(false);
	}
	
	public void ClickChoiceButton(string buttonName, GameObject messageTargetGO, string messageName)
	{
		Debug.Log(buttonName);
		messageTargetGO.SendMessage(messageName,buttonName);
		HideChoices();
	}
	
	public void ClickDialogButton(string buttonName, GameObject messageTargetGO, string messageName)
	{
		Debug.Log(buttonName);
		messageTargetGO.SendMessage(messageName,buttonName);
		HideDialog();
	}
	
	public void ShowDialog(string dialogTitle, string dialogMessage, GameObject messageGO, string messageName)
	{
		HideChoices();
		Debug.Log("setActive");
		dialogGO.SetActive(true);
		dialogTitleText.text = dialogTitle;
		dialogText.text = dialogMessage;
		string buttonName = "OK";
		dialogDismissButton.onClick.AddListener( () => ClickDialogButton(buttonName,messageGO,messageName));
	}
	
	public void HideDialog()
	{
		dialogGO.SetActive(false);
	}
	
	
	
	
	// Use this for initialization
	void Start () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	}
}
