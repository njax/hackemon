﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; // Allows us to use collections which are resizeable dynamic arrays such as List and Dictionary

namespace FindableV1{
public class Findables : MonoBehaviour {

	/// <summary>
	/// Minimum of seconds between each chance for a Findable to spawn near a player
	/// </summary>
	public float findableRate = 30f;

	[System.Serializable]
	public class Findable
	{
		public string name = "Findable";
		/// <summary>
		/// The rarity (0-1) every findableRate seconds
		/// </summary>
		public float rarity = 0.1f;
		/// <summary>
		/// location types where the findable can be found 
		/// </summary>
		public List<string> locationTypes;
		/// <summary>
		/// The specific location of this particular Findable once it has been spotted to be found
		/// </summary>
		public Geolocation location = new Geolocation();
	}

	// Declare a public variable that is a list of Findables called possibleFindables
	public List<Findable> possibleFindables = new List<Findable>();


	[System.Serializable]
	public class Geolocation
	{
		/// <summary>
		/// lattitude/longitude
		/// </summary>
		public Vector2 gps = new Vector2();
		public string streetAddress = "0 Street Name, Area, CA 94000";
		public string countryName = "USA";
		public int streetNumber = 0;
		public string streetName = "Street Name";
		public string cityName = "Area";
		public string countyName = "County";
		public string stateName = "California";
		public string stateAbbreviation = "CA";
		public int areaCode = 94000;
	}

	[System.Serializable]
	public class Player
	{
		public string playerName = "Anonymous";
		public Geolocation location = new Geolocation();
		/// <summary>
		/// The findables aka sweet loots the player has
		/// </summary>
		public List<Findable> findables = new List<Findable>();
	}


	public List<Player> players = new List<Player>();

	/// <summary>
	/// The current controlling player of the game client.
	/// </summary>
	public Player player = new Player();


	#region gameplay

	/// <summary>
	/// Player finds a sweet loot
	/// </summary>
	public void Find()
	{

		// We need to keep track of which findable has the best winning rarity so far
		Findable findableWithLowestRaritySoFar = new Findable();
		findableWithLowestRaritySoFar.rarity = 1f;

		// First we generate a rarity roll, a random number between 0 and 1, and store it in a variable
		float randomNumber = Random.Range(0f, 1f);
		// Then we foreach loop through all the possibleFindable objects for this location type

		// for each Findable object called f in the possibleFindable list, do the following:
		foreach (Findable eachFindable in possibleFindables)
		{

		}
		// for each Findable object called f in the possibleFindable list, do the following:
		foreach (Findable eachFindable in possibleFindables)
		{
			// Is this findable's rarity lower than our random number roll?
			if (randomNumber < eachFindable.rarity)
			{
				// Check if the rarity of the findable is beaten by our roll
				if (findableWithLowestRaritySoFar == null 
					|| eachFindable.rarity < findableWithLowestRaritySoFar.rarity)
				{
					// We have a new rarity champion!
					findableWithLowestRaritySoFar = eachFindable;
				}
			}
		}
		if (findableWithLowestRaritySoFar.rarity != 1f)
		{
		// If we found a loot, great, add it to our list
		// When we reach the rarest loot that our roll beats, we get that loot
			// TODO: THIS IS TOTALLY WRONG AND SHOULD INCLUDE THE NEW INSTANCE CODE
			player.findables.Add(findableWithLowestRaritySoFar);
		}
		// If not, we just wait till the next chance after the loot period comes again
		
	}

	public void OtherPlayerFindsLoot()
	{

	}

	/// <summary>
	/// Player compares whose loot is the best with another player
	/// </summary>
	public void Battle()
	{

	}

	/// <summary>
	/// 2 players offer loots to accept a trade
	/// </summary>
	public void Trade()
	{

	}
	#endregion

	#region MonoBehaviour
	// Use this for initialization
	void Start () {
		Find();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	#endregion
} // class
} // namespace