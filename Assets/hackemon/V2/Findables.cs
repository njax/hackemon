﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; // Allows us to use collections which are resizeable dynamic arrays such as List and Dictionary

// NB hackatorium: 37.762416,-122.4214747
// NB classrooms: 37.7624295,-122.4190934
namespace FindableV2{
public class Findables : MonoBehaviour {

	/// <summary>
	/// Minimum of seconds between each chance for a Findable to spawn near a player
	/// </summary>
	public float findableRate = 30f;
	public float timeLastFound = 0f;
	public float timeNextFound = 0f;

	[System.Serializable]
	public class Findable
	{
		public string name = "Findable";
		/// <summary>
		/// The rarity (0-1) every findableRate seconds
		/// </summary>
		public float rarity = 0.1f;
		/// <summary>
		/// location types where the findable can be found 
		/// </summary>
		public List<string> locationTypes;
		/// <summary>
		/// The specific location of this particular Findable once it has been spotted to be found
		/// </summary>
		public Geolocation location = new Geolocation();
		public int level = 1;
		public int hp = 10;
		public int hpMax = 10;
		public FindableType type;
	}
	
	public enum FindableType
	{
		Normal,
		Water,
		Fire,
		Fictional,
		Historical
	}

	// Declare a public variable that is a list of Findables called possibleFindables
	public List<Findable> possibleFindables = new List<Findable>();
	
	

	[System.Serializable]
	public class Geolocation
	{
		/// <summary>
		/// lattitude/longitude
		/// </summary>
		public Vector2 gps = new Vector2();
		public string streetAddress = "0 Street Name, Area, CA 94000";
		public string countryName = "USA";
		public int streetNumber = 0;
		public string streetName = "Street Name";
		public string cityName = "Area";
		public string countyName = "County";
		public string stateName = "California";
		public string stateAbbreviation = "CA";
		public int areaCode = 94000;
	}
	
	[System.Serializable]
	public class Hotspot 
	{
		public string name = "Noisebridge";
		public string[] placeTypes;
		public Geolocation location;
	}
	
	public List<Hotspot> spots = new List<Hotspot>();

	[System.Serializable]
	public class Player
	{
		public string playerName = "Anonymous";
		public Geolocation location = new Geolocation();
		public Hotspot spot = new Hotspot();
		/// <summary>
		/// The findables aka sweet loots the player has
		/// </summary>
		public List<Findable> findables = new List<Findable>();
	}


	public List<Player> players = new List<Player>();

	/// <summary>
	/// The current controlling player of the game client.
	/// </summary>
	public Player player = new Player();


	#region gameplay

	/// <summary>
	/// Player finds a sweet loot matching the location type
	/// </summary>
	public void Find(Player findingPlayer)
	{
		bool findablesMatchingSpot = false;
		List<Findable> findablesMatchingSpotList = new List<Findable>();
		// for each Findable object called f in the possibleFindable list, do the following:
		foreach (Findable eachFindable in possibleFindables)
		{
			Findable thisFindable = eachFindable;
			
			foreach (string eachFindabletype in eachFindable.locationTypes)
			{
				string type = eachFindabletype;
				foreach (string eachSpotType in findingPlayer.spot.placeTypes )
				{
					if (eachSpotType == eachFindabletype)
					{
						findablesMatchingSpot = true;
						findablesMatchingSpotList.Add( thisFindable );
					}
				}
			}
		}
		
		if (findablesMatchingSpot)
		{
			Findable found = findablesMatchingSpotList[Random.Range(0,findablesMatchingSpotList.Count)];
			
			// If we found a loot, great, add it to our list
			Findable newFindable = new Findable();
			newFindable.name = found.name;
			newFindable.hp = found.hp;
			newFindable.level = found.level;
			newFindable.type = found.type;
			findingPlayer.findables.Add(newFindable);
		}
	}
	
	public void FindUpdate()
	{
		if (Time.time > timeNextFound)
		{
			player.location.gps = PickRandomLocation();
			player.spot = FindNearestSpot(player.location.gps);
			Find(player);
			foreach (Player thisPlayerFindingSomething in players)
			{
				thisPlayerFindingSomething.location.gps = PickRandomLocation();
				thisPlayerFindingSomething.spot = FindNearestSpot(player.location.gps);
				
				Find(thisPlayerFindingSomething);
			}
			BattleMenu();
			timeLastFound = Time.time;
			timeNextFound = Time.time + findableRate + Random.Range(0,findableRate);
		}
	}
	
	public Vector2 PickRandomLocation()
	{
		Hotspot randomSpot = spots[Random.Range(0,spots.Count)];
		return new Vector2( randomSpot.location.gps.x, randomSpot.location.gps.y );
	}
	
	public Hotspot FindNearestSpot(Vector2 gps)
	{
		float closestSpotDistance = float.PositiveInfinity;
		Hotspot closestSpotSoFar = new Hotspot();
		
		foreach (Hotspot s in spots)
		{
			float sDistance = Vector2.Distance(gps,s.location.gps);
			if (sDistance < closestSpotDistance)
			{
				closestSpotDistance = sDistance;
				closestSpotSoFar = s;
			}
		}
		if (closestSpotSoFar.location.gps.x != 0f)
		{
			return closestSpotSoFar;
		}
		return null;
	}

	/// <summary>
	/// Player compares whose loot is the best with another player
	/// </summary>
	public void Battle(Player playerA, Player playerB)
	{
		Findable pickA = ChooseRandom(playerA);
		Findable pickB = ChooseRandom(playerB);
		
		bool winnerA = false;
		bool winnerB = false;
		bool battleOver = false;
		int maxRounds = 100;
		int roundCount = 0;
		while (!battleOver && roundCount < maxRounds)
		{
			ShowFindable(playerA,pickA);
			ShowFindable(playerB,pickB);
			
			Attack(pickA,pickB);
			Attack(pickB,pickA);
			
			//pickB.hp = 0;
			
			if (pickB.hp <= 0 && pickA.hp <= 0)
			{
				battleOver = true;
				Debug.Log("DRAW (Both are exhausted)");
			}
			if (pickB.hp <= 0)
			{
				battleOver = true;
				winnerB = true;
				Debug.Log(pickA.name + " beat " + pickB.name);
				LevelUp(pickA);
				pickB.hp = pickB.hpMax;
			}
			if (pickA.hp <= 0)
			{
				battleOver = true;
				winnerA = true;
				Debug.Log(pickB.name + " beat " + pickA.name);
				LevelUp(pickB);
				pickA.hp = pickA.hpMax;
				
			}
			roundCount ++;
		}
		BattleMenu();
	}
	
	void BattleUpdate ()
	{
		int playerIndex = 0;
		// Let the player hit a key to challenge another player.
		foreach (Player eachPlayer in players)
		{
			playerIndex++;
			
			if ( Input.GetKeyDown(""+(playerIndex)))
			{
				Battle(player, eachPlayer);
			}
		}
		
	}
	
	void BattleMenu()
	{
		int playerIndex = 0;
		// Let the player hit a key to challenge another player.
		foreach (Player eachPlayer in players)
		{
			playerIndex++;
			Debug.Log (playerIndex + ":" + eachPlayer.playerName);
		}
	}
	
	public void LevelUp(Findable leveler)
	{
		leveler.level ++;
		leveler.hpMax += 10;
		leveler.hp = leveler.hpMax;
		Debug.Log(leveler.name + " levels up to level " + leveler.level + " with " + leveler.hpMax + "HP!");
	}
	
	// A public function that returns nothing called Attack takes 2 arguments:
	// A findable called attacker and a Findale called defender.
	public void Attack (Findable attacker, Findable defender)
	{
		// int max is exclusive
		int howMuchDamage = Random.Range(1,attacker.level+2);
		defender.hp -= howMuchDamage;
		Debug.Log(attacker.name + " does " + howMuchDamage + " to " + defender.name);
	}
	
	public void ShowFindable(Player owner, Findable it)
	{
		// Eg. "Ash:Pikachu L:5 HP:40/50"
		string hitPointsString = " HP:" + it.hp + "/" + it.hpMax;
		Debug.Log(owner.playerName + ":" + it.name + " L:" + it.level + hitPointsString  );
	}
	
	public Findable ChooseRandom (Player chooser)
	{
		if (chooser.findables.Count==0)
		{
			Debug.Log(chooser.playerName + " has none to choose from!");
		}
		int findablesIndexMax = chooser.findables.Count-1;
		// int max is exclusive
		int findableIndexFound = Random.Range(0,findablesIndexMax+1);
		Debug.Log("findableIndexFound:"+findableIndexFound+" out of " + chooser.findables.Count);
		Findable choice = chooser.findables[findableIndexFound];
		return choice;
	}

	/// <summary>
	/// 2 players offer loots to accept a trade
	/// </summary>
	public void Trade()
	{

	}
	#endregion

	#region MonoBehaviour
	// Use this for initialization
	void Start ()
	{
		//Find();
		
	}
	
	// Update is called once per frame
	void Update () {
		FindUpdate();
		BattleUpdate();
	}
	#endregion
} // class
} // namespace