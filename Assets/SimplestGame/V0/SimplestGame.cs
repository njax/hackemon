﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SimplestGameV0 {
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// == VARIABLES
	/// === INT
	/// points
	/// === BOOL
	/// win
	/// == FUNCTION
	/// CheckGoal()
	/// == ENUM
	/// KeyCode
	/// == UNITY API CLASSES
	/// Input
	/// == UNITY API CLASS FUNCTIONS
	/// Input.GetKeyDown
	/// == DOT NOTATION
	/// </remarks>
	public class SimplestGame : MonoBehaviour {
		public string version = "0.0";
		public int points = 0;
		public int pointsGoal = 10;
		public bool win = false;
		public KeyCode keyToPress = KeyCode.Space;
		
		void CheckGoal()
		{
			if (points >= pointsGoal)
			{
				win = true;
			}
		}
		
		// Update is called once per frame
		void Update () 
		{
			if (Input.GetKeyDown(keyToPress))
			{
				points++;
			}
			
			CheckGoal();
		}
	}
}
